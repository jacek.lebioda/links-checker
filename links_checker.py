"""
The script checks, if the links in HTML or markdown files are accessible.
Requires: `beautifulsoup4`, `httplib2`
"""

import httplib2
import os

from bs4 import BeautifulSoup


DEFAULT_USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'


class FilesDiscoverer:
    # Discovers the files specified directory (by default, working directory).

    def _should_pick_the_file(directory, filename):
        # Decides if the file should be taken further into processing
        correct_extensions = [
            '.html',
            '.md',
            '.htm',
            '.markdown'
        ]
        return any([filename.endswith(ext) for ext in correct_extensions])

    def discover_files(self,
                       directory=None,
                       filter_func=_should_pick_the_file):
        # Finds in the directory the files matching the `filter_func` predicate
        files = []
        directory = directory if directory is not None else '.'
        for (dir_path, _, file_names) in os.walk(directory):
            for filename in file_names:
                if filter_func(dir_path, filename):
                    el = (dir_path, os.path.join(directory, dir_path, filename))
                    files.append(el)
        return files


class HtmlLinkPicker:
    def __init__(self, file, whitelist=None, parser_type=None):
        self.file = file
        self.parser_type = 'html.parser' if parser_type is None else parser_type
        self.whitelist = whitelist if whitelist is not None else []

    def should_include_link(self, el):
        return not el.startswith('#') and \
               not el.startswith('mailto:') and \
               not el.startswith('ftp://') and \
               not el in self.whitelist

    def process_file(self):
        links = []
        with open(self.file, 'r', encoding="utf8") as f:
            contents = f.read()
            parser = BeautifulSoup(contents, self.parser_type)
            for link in parser.findAll('a'):
                link_address = link.get('href', '')
                if not self.should_include_link(link_address):
                    continue
                if link_address not in links:
                    links.append(link_address)
        return links
