# A python script to check if the links in your project lead somewhere
Running the script will:
 * scan the files for HTML anchors (`<a href="http://the-example-link.com">the label</a>`) and markdown links (`[the label](http://the-example-link.com)`)
 * check, if visiting them yields successful response
 * report back in case of any problems

# Requirements
The script requires `beautifulsoup4` and `httplib2` to be installed.
Use `pip install beautifulsoup4 httplib2` to install them.

# Example _Gitlab-CI_ configuration
Make sure that `build` stage exists, and generates HTML files.
```
test_paths:
  image: python:3.7.2-stretch
  stage: test
  script:
  - pip3 install beautifulsoup4 httplib2
  - python3 links_checker.py
```
